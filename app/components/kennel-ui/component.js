import { alias, not } from '@ember/object/computed';
import Component from '@ember/component';
import { htmlSafe } from '@ember/template';
import { computed } from '@ember/object';
import arePropertiesNotEqual from 'calendar/macros/equalProperties';

export default Component.extend({
  tagName: '',

  title: alias('model.kennel_type'),

  isNotSelected: arePropertiesNotEqual('draggedReservation.idkennel.number', 'model.number'),
  isSelected: not('isNotSelected'),

  style: computed('model.color', function() {
    return htmlSafe(`background-color: ${this.get('model.color')}`);
  }),

  href: computed('model.idkennel', function() {
    return `/admin/kennel-types/edit/${this.model.idkennel_type}`;
  }),
});
