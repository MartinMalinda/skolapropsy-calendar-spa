import Component from '@ember/component';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import moment from 'moment';
import { computed } from '@ember/object';

export default Component.extend({
  dateHelp: service(),

  attributeBindings: ['tabindex'],
  tabindex: 0,

  reservationDates: computed('reservation.{valid_from,valid_to}', function() {
    let start = this.get('reservation.valid_from');
    let end = this.get('reservation.valid_to');

    return this.get('dateHelp').getArrayOfDates(start, end);
  }),

  keyUp(e) {
    if (e.keyCode === 27) {
      this.send('rollback');
    }
  },

  didInsertElement() {
    this._super(...arguments);
    this.set('previousActiveElement', document.activeElement);
    this.$().focus();
  },

  actions: {
    cut() {
      let selectedDate = $($('[data-cut-select]')[0]).val();
      let formatedDate = moment(selectedDate).format('YYYY-MM-DD');

      let reservation = this.get('reservation');

      let newReservationData = reservation.toJSON();

      newReservationData.idkennel = reservation.get('idkennel');
      newReservationData.valid_from = moment(selectedDate).add(1, 'day');
      reservation.set('valid_to', formatedDate);

      this.get('onCreate')(newReservationData);
    },

    rollback() {
      this.get('reservation').rollback();
      this.get('close')();
      this.$(this.get('previousActiveElement')).focus();
    },

    async saveSelectedReservation() {
      let res = this.get('reservation');
      await res.save();
      res.didLoad();
      this.get('close')();
    },

    deleteSelectedReservation() {
      if (confirm('Opravdu odstranit?')) {
        let res = this.get('reservation');
        res.set('status', 2);
        res.save().then(() => {
          res.deleteRecord();
          this.get('close')();

        });
      }
    },

    updateOverlappings() {
      this.get('reservation').updateOverlappings();
    }
  }
});
