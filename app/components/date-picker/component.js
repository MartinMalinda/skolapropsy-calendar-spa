import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({

  tagName: 'span',

  didInsertElement() {
    let datepicker = $(this.element).find('input');

    datepicker.daterangepicker({

      singleDatePicker: true,
      startDate: this.get('value'),
      dateFormat: 'YYYY-MM-DD',
      format: 'YYYY-MM-DD',

      locale: {
        format: 'YYYY-MM-DD',

        daysOfWeek: ['Po', 'Ut', 'Str', 'Ct', 'Pa', 'So', 'Ne'],
        monthNames: ['Leden', 'Unor', 'Brezen', 'Duben', 'Kveten', 'Cerven', 'Cervenec', 'Srpen', 'Zari', 'Rijen', 'Listopad', 'Prosinec']
      }
    }, () => {
      this.get('onChange')();
    }

    );
  },

  willDestroyElement() {
    $('.datepicker').unbind();
  }
});
