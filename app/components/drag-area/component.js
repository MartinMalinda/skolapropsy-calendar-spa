import config from 'calendar/config/environment';
import Component from '@ember/component';
import { computed } from '@ember/object';
import $ from 'jquery';
import { run } from '@ember/runloop';

export default Component.extend({
  classNames: ['drag-area'],
  draggedReservation: null,
  offsetY: null,

  mappedKennels: computed('kennels', function() {
    const mappedKennels = [];
    this.get('kennels').forEach((kennel) => {
      mappedKennels[kennel.get('number')] = kennel;
    });

    return mappedKennels;
  }),

  mouseMove(event) {
    const reservation = this.get('draggedReservation');
    if (reservation) {
      const offsetY = this.get('offsetY');
      let y = event.pageY - offsetY;

      if (!y) {
        y = event.originalEvent.changedTouches[0].pageY - offsetY;
        event.originalEvent.preventDefault();
      }

      const kennelNumber = Math.floor(y / config.rowHeight) + 1;
      if (kennelNumber !== reservation.get('kennel.number')) {
        const kennel = this.get('mappedKennels')[kennelNumber];
        if (kennel) {
          reservation.set('idkennel', kennel);
        }
      }
    }

  },
  touchMove(event) {
    this.mouseMove(event);
  },
  touchEnd() {
    this.mouseUp();
  },
  touchCancel() {
    this.mouseUp();
  },

  mouseUp() {
    this.afterDrag();
  },

  mouseLeave() {
    this.afterDrag();
  },

  afterDrag() {
    const reservation = this.get('draggedReservation');
    if (reservation) {
      reservation.updateOverlappings();
      this.set('draggedReservation', null);
    }
  },

  calcPositions() {
    const $columns = $('.top-row, .columns');
    const $coteCells = $('.cote-cells');
    const $window = $(window);
    window.requestAnimationFrame(() => {
      let posY = $window.scrollTop();
      if (posY < 40) {
        posY = 40;
      }

      $columns.css('top', posY - 41);
      $coteCells.css('left', $window.scrollLeft());
      this.calcPositions();
    });
  },

  didInsertElement() {
    this.calcPositions();

    if (config.domInit) {
      window.domInit();
    }

    run.later(()=> {
      this.set('offsetY', this.$().offset().top);
    }, 200);
  }
});
