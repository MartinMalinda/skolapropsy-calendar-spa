import ModalDialog from 'ember-modal-dialog/components/modal-dialog';

export default ModalDialog.extend({
  translucentOverlay: true,
  hasOverlay: true,
  containerClassNames: 'modal-prototype',

  tagName: 'div'
});
