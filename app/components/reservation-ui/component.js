import config from 'calendar/config/environment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { run } from '@ember/runloop';
import $ from 'jquery';
import { htmlSafe } from '@ember/string';
import Component from '@ember/component';
import moment from 'moment';

export default Component.extend({
  attributeBindings: ['style', 'tabindex'],
  classNames: ['reservation'],
  classNameBindings: ['model.looksDirty:dirty', 'unconfirmed'],
  isMoving: false,
  draggedReservation: null,
  clickCoordinates: null,

  tabindex: computed('model.{idkennel,valid_from}', function() {
    return `${this.get('model.kennel.id') * 100}${moment(this.get('valid_from')).dayOfYear()}`;
  }),

  dateHelp: service(),

  isVisible: computed('model.status', function() {
    return this.get('model.status') !== 2;
  }),

  topOffset: computed('model.kennel.number', function() {
    return this.get('model.kennel.number') * config.rowHeight - 30;
  }),

  numberOfDays: computed('model.{valid_from,valid_to}', function() {
    return this.get('dateHelp').dateDiff(this.get('model.valid_from'), this.get('model.valid_to'));
  }),

  width: computed('model.{valid_from,valid_to}', function() {
    const numberOfDays = this.get('numberOfDays');
    return (numberOfDays + 1) * config.colWidth;

  }),

  leftOffset: computed('model.valid_from', 'previousMonth', 'nextMonth', function() {
    const startingDate = moment(this.get('model.valid_from'));
    const currentDate = moment([this.get('year'), this.get('month') - 1, 1]);
    const diff = this.get('dateHelp').dateDiff(currentDate, startingDate) + 1;
    return diff * config.colWidth; // %
  }),

  zIndex: computed('numberOfDays', function() {
    return 300 - this.get('numberOfDays');
  }),

  style: computed('width', 'leftOffset', 'topOffset', function() {
    return htmlSafe(`left: ${this.get('leftOffset')}px; top: ${this.get('topOffset')}px; width: ${this.get('width')}px;z-index: ${this.get('zIndex')}`);
  }),

  getDistance(point1, point2) {
    if (point1 && point2) {
      return Math.sqrt(
        Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2)
      );
    } else {
      return null;
    }
  },

  getCoordinates(event) {
    return {
      x: event.pageX,
      y: event.pageY
    };
  },

  keyUp(e) {
    if (e.keyCode === 13) {
      this.showReservation(this.get('model'));
    }
  },

  mouseUp() {
    this.set('dragging', false);
    if (!this.get('draggedReservation')) {
      this.showReservation(this.get('model'));
    }

    this.set('draggedReservation', null);
    this.get('model').updateOverlappings();
  },
  mouseDown(event) {
    if (event) {
      this.set('clickCoordinates', this.getCoordinates(event));
    }
    this.set('dragging', true);
  },
  mouseMove(event) {
    if (!this.get('draggedReservation')) {
      if (event) {
        const point1 = this.get('clickCoordinates');
        const point2 = this.getCoordinates(event);
        const delta = this.getDistance(point1, point2);

        if (this.get('dragging') && delta > 5) {
          this.setDraggedReservation(this.get('model'));
        }
      } else {
        this.setDraggedReservation(this.get('model'));
      }
    }
  },

  touchStart(event) {
    event.preventDefault();
    this.mouseDown();
  },
  touchMove() {
    this.mouseMove();
  },
  touchEnd() {
    this.mouseUp();
  },
  touchCancel() {
    this.mouseUp();
  },

  didReceiveAttrs() {
    if (!this.get('draggedReservation')) {
      this.set('dragging', false);
    }
  },

  didRender() {
    if (this.get('unconfirmed')) {
      const left = this.get('leftOffset') - 60;
      run.later(() => {
        if (!this.get('didScroll')) {
          $('body').scrollLeft(left);
          this.set('didScroll', true);
        }

      }, 500);
    }
  }
});
