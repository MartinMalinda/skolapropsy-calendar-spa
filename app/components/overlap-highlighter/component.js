import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { htmlSafe } from '@ember/template';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['overlap-highlighter'],
  attributeBindings: ['style'],

  dateHelp: service(),

  leftOffset: computed('startDate', 'interval.start', function() {
    return this.get('dateHelp').dateDiff(this.get('startDate'), this.get('interval.start')) * 40;
  }),

  width: computed('interval', function() {
    return (this.get('dateHelp').dateDiff(this.get('interval.start'), this.get('interval.end')) + 1) * 40 - 2;
  }),

  style: computed('leftOffset', function() {
    return htmlSafe(`left:${this.get('leftOffset')}px;
						 width:${this.get('width')}px`);
  })
});
