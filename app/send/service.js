import Service from '@ember/service';
import $ from 'jquery';

export default Service.extend({
  async makeSendRequest(reservationId, response, confirm) {
    const data = { reservation: reservationId, response, confirm };

    try {
      await $.ajax({
        type: 'POST',
        url: '/admin/calendar/api/send',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(data)
      });

      return {
        message: 'Email byl odeslan.',
        success: true
      }
    } catch(e) {
      return {
        message: 'Pri odeslani emailu doslo k chybe',
        success: false
      }
    }
  }
});
