import { helper } from '@ember/component/helper';

export function propMut([model, propName, value]) {
  model.set('propName', value);
  return value;
}

export default helper(propMut);
