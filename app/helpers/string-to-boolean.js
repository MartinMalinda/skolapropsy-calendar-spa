import { helper } from '@ember/component/helper';

export function stringToBoolean([model, propName]) {
  return function(str) {
    if (str === 'ano') {
      return model.set(propName, true);
    }

    return model.set(propName, false);
  }
}

export default helper(stringToBoolean);
