import { helper as buildHelper } from '@ember/component/helper';

export function translate([value, lang = 'cz', option]) {

  function boolToString(bool, lang) {
    let data = {
      yes: {
        cz: 'Ano',
        en: 'Yes'
      },
      no: {
        cz: 'Ne',
        en: 'No'
      }
    };

    if (bool) {
      return data.yes[lang];
    } else {
      return data.no[lang];
    }
  }

  let translations = {
    'dopoledne': {
      'bring': {
        en: 'Between 9th and 11th hour of first day of reservation',
        cz: 'Mezi 9. a 11. hodinou v prvním dni rezervace'
      },
      'leave': {
        en: 'Between 9th and 11th hour of last day of reservation',
        cz: 'Mezi 9. a 11. hodinou v poslední den rezervace'
      }
    },
    'odpoledne': {
      'bring': {
        en: 'Between 17th and 19th hour of first day of reservation',
        cz: 'Mezi 17. a 19. hodinou v prvním dni rezervace'
      },
      'leave': {
        en: 'Between 17th and 19th hour of the last day of reservation',
        cz: 'Mezi 17. a 19. hodinou v poslední den rezervace'
      }
    },
    'vyzvednout': {
      'bring': {
        en: 'I would like to use your services, that we come over for the dog',
        cz: 'Využiji vaší služby, kdy pro pejska přijedete'
      },
      'leave': {
        en: 'I would like to use your services to bring the dog to me.',
        cz: 'Využiji vaší služby, kdy mi pejska přivezete zpět'
      }
    }
  };

  if (typeof value === 'boolean') {
    return boolToString(value, lang);
  }

  return translations[value][option][lang];

}

export default buildHelper(translate);
