import { helper } from '@ember/component/helper';

export function toJSON([objOrModel]) {
  const obj = objOrModel.toJSON ? objOrModel.toJSON() : objOrModel;
  return JSON.stringify(obj);
}

export default helper(toJSON);
