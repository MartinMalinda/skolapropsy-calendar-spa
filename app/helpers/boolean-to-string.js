import { helper } from '@ember/component/helper';

export function booleanToString([bool]) {
  if (bool) {
    return 'ano';
  }

  return 'ne';
}

export default helper(booleanToString);
