import Controller from '@ember/controller';
import EmberObject, { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';
import $ from 'jquery';

function removeVat(price) {
  return Math.round((price/1.21));
};

const ExtraField = EmberObject.extend({
  price: '0',

  priceWithoutVat: computed('price', function() {
    if (this.price) {
      return removeVat(this.price).toFixed(2);
    }

    return '0.00';
  }),

  taxValue: computed('price', function() {
    return this.price - Number(this.priceWithoutVat);
  }),

  width: computed('price', function() {
    if (this.price) {
      return (String(this.price).length + 1.5) * 8.7;
    }

    return 20;
  }),
});

export default Controller.extend({
  store: service(),
  price: service(),

  init() {
    this._super(...arguments);
    this.set('extraFields', []);
    this.set('kennels', this.store.peekAll('kennel'));
  },

  currentMonth: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('M');
  }),

  currentYear: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('YYYY');
  }),

  today: moment().format('DD. MM. YYYY'),
  dueDate: moment().add('21', 'day').format('DD. MM. YYYY'),

  kennel: computed('model.kennel_type', function() {
    return this.kennels.findBy('kennel_type', this.model.kennel_type);
  }),

  pricePerDay: computed('kennel.price', 'kennel.outdoor_enclosure', 'model.heating', 'model.outdoor_enclosure', 'model.drugs', 'model.quarantine', function() {
   return this.price.calcFullPricePerDay(this.get('model'));
  }),

  priceWithVat: computed('pricePerDay', 'model.daysLength', function() {
    return this.model.daysLength * this.pricePerDay;
  }),

  vatAmount: computed('priceWithVat', function() {
    return this.priceWithVat - Number(this.priceWithoutVat);
  }),

  priceWithoutVat: computed('priceWithVat', function() {
    return removeVat(this.get('priceWithVat'));
  }),

  totalPriceWithVat: computed('priceWithVat', 'extraFields.@each.price', function() {
    return this.priceWithVat + this.extraFields.reduce((priceFromExtraFields, extraField) => {
      return priceFromExtraFields + Number(extraField.price);
    }, 0);
  }),

  actions: {
    setBoolean(model, prop, string) {
      model.set(prop, string === 'ano');
    },

    addExtraField() {
      this.extraFields.pushObject(ExtraField.create({
        name: 'Nova polozka',
        details: 'Popis'
      }));
    },

    removeExtraField(extraField) {
      this.extraFields.removeObject(extraField);
    },

    async setAsPaid() {
      const url = `/admin/invoice/${this.model.id}?issue_invoice=1`;
      try {
        await $.post(url);
        this.set('model.issued_invoice', true);
      } catch(e) {
        alert('Pri aktualizaci rezervace doslo k chybe', e);
      }

    }
  }
});
