import DS from 'ember-data';

const { attr } = DS;

export default DS.Model.extend({
  name: attr('string'),
  price: attr('string'),
  outdoor_enclosure: attr('string')
});
