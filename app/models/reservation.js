import { alias, or, equal, filter, map } from '@ember/object/computed';
import DS from 'ember-data';
import arePropertiesNotEqual from 'calendar/macros/equalProperties';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';

const { attr, belongsTo } = DS;

export default DS.Model.extend({
  dog_name: attr('string'),
  valid_from: attr('string'),
  valid_to: attr('string'),
  breed: attr('string'),
  idcont: attr('number'),
  // idkennel: attr('string'),
  idkennel: belongsTo('kennel'),
  kennel_type: attr('string'),
  owner: attr('string'),
  phone: attr('string'),
  note: attr('string'),
  type_of_bring: attr('string'),
  type_of_leave: attr('string'),
  status: attr('number'),

  outdoor_enclosure: attr('boolean'),
  heating: attr('boolean'),
  drugs: attr('boolean'),
  quarantine: attr('boolean'),
  issued_invoice: attr('boolean'),

  statestamp: attr('date'),
  createstamp: attr('date'),

  customer_address: attr(),

  price: attr(),

  lang: attr('string'),

  dateHelp: service(),
  priceService: service('price'),

  kennel: alias('idkennel'),

  hasBuiltInEnclosure: equal('kennel_type', 'Kotec pro malé psy (s výběhem)'),

  hasNewKennel: arePropertiesNotEqual('idkennel.number', 'idkennelCache.number'),
  looksDirty: or('hasDirtyAttributes', 'hasNewKennel'),

  hasBuiltInEnclosure: computed('kennel_type', function() {
    return this.get('kennel_type').includes('výběh');
  }),

  isUnconfirmed: equal('status', 0),

  isCz: equal('lang', 'cz'),

  dateRange: computed('valid_from', 'valid_to', function() {
    let from = moment(this.get('valid_from'));
    let to = moment(this.get('valid_to'));
    return moment.range(from, to);
  }),

  reservationsInSameKennel: computed(function() {
    let reservations = this.get('kennel.reservations');
    if (reservations) {
      return reservations.without(this);
    } else {
      return [];
    }
  }),

  daysLength: computed('dateRange', function() {
    return this.get('dateRange').diff('day') + 1;
  }),

  basePrice: computed('daysLength', 'price', function() {
    return this.get('daysLength') * this.get('price');
  }),

  totalPrice: computed('daysLength', 'price', function() {
    return this.get('priceService').calculatePrice(this, this.get('idkennel'));
  }),

  priceWithoutVat: computed('totalPrice', function() {
    return this.get('priceService').calcPriceWithoutVat(this.get('totalPrice'));
  }),

  overlappingWith: filter('reservationsInSameKennel', function(reservation) {
    // return reservation.get('dateRange').overlaps(this.get('dateRange'));
    return this.get('dateHelp').overlaps(this.get('dateRange'), reservation.get('dateRange'));
  }),

  overlappingIntervals: map('overlappingWith', function(reservation) {

    // return this.get('dateRange').intersect(reservation.get('dateRange'));
    return this.get('dateHelp').intersect(this.get('dateRange'), reservation.get('dateRange'));
  }),

  overlappingDays: computed('overlappingIntervals', function() {
    let intervals = this.get('overlappingIntervals');
    let sum = 0;
    intervals.forEach((interval) => {
      sum = sum + interval.diff('days') + 1;
    });

    return sum;
  }),

  updateOverlappings() {
    let currentKennelReservations = this.get('reservationsInSameKennel').toArray();
    this.notifyPropertyChange('reservationsInSameKennel');
    let newKennelReservations = this.get('reservationsInSameKennel').toArray();

    let allRelevantReservations = [];
    allRelevantReservations.pushObjects(currentKennelReservations);
    allRelevantReservations.pushObjects(newKennelReservations);

    allRelevantReservations.forEach((reservation) => {
      reservation.notifyPropertyChange('reservationsInSameKennel');
    });
  },

  didLoad() {
    setTimeout(() => {
      this.get('idkennel').then((idkennel) => {
        this.set('idkennelCache', idkennel);
      }, 600);
    });
  },
  rolledBack() {
    this.set('idkennel', this.get('idkennelCache'));
  },
  rollback() {
    this.rollbackAttributes();
    this.rolledBack();
  }
});
