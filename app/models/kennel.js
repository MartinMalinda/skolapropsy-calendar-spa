import { equal } from '@ember/object/computed';
import { computed } from '@ember/object';
import DS from 'ember-data';

const { attr, hasMany } = DS;

export default DS.Model.extend({
  name: attr('string'),
  kennel_type: attr('string'),
  idkennel_type: attr('string'),
  color: attr('string'),

  price: attr('number'),

  number: computed('name', function() {
    if (this.get('name')) {
      return Number(this.get('name').split(' ')[1]);
    } else {
      return false;
    }
  }),

  outdoor_enclosure: attr('boolean'),

  reservations: hasMany('reservation'),

  hasBuiltInEnclosure: equal('kennel_type', 'Kotec pro malé psy (s výběhem)')
});
