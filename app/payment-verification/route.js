import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    return this.store.findRecord('reservation', params.id);
  },

  actions: {
    error() {
      this.transitionTo('index', 1, 2016);
    }
  }
});
