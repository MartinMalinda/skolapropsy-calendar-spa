import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
  sendService: service('send'),  

  init() {
    this._super(...arguments);
    
    this.set('message', {});
  },

  currentMonth: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('M');
  }),

  currentYear: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('YYYY');
  }),

  actions: {
    async send() {
      const reservationId = this.model.id;
      const response = $('.response').val();
      const confirm = false;
      const { message, success } = await this.sendService.makeSendRequest(reservationId, response, confirm);
      this.set('message.content', message);
      if (success) {
        this.set('message.class', 'success');
      } else {
        this.set('message.class', 'error');
      }
    }
  }
});
