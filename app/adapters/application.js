import DS from 'ember-data';
import config from 'calendar/config/environment';

export default DS.RESTAdapter.extend({
  host: config.endpoint,
  namespace: 'api',

  urlForFindRecord(id, modelName) {
    let url = this.buildURL(modelName, id);
    return url.replace('reservations', 'reservation');
  }

});
