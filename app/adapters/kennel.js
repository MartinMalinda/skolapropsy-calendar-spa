import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  findRecord(store, type, id) {
    if (id > 0) {
      let json = store.peekRecord('kennel', id).toJSON();
      json.id = id;

      return { kennel: json };
    } else {
      return {};
    }
  }
});
