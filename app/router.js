import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('index', { path: '/:month/:year' });
  this.route('test');
  this.route('confirm', { path: '/confirm/:id' });
  this.route('invoice', { path: '/invoice/:id' });
  this.route('payment-verification', { path: '/payment-verification/:id' });
});

export default Router;
