import Service from '@ember/service';

export default Service.extend({
  calcFullPricePerDay(reservation) {
    let pricePerDay = reservation.price;
    const { heating, outdoor_enclosure, drugs, quarantine, kennel, hasBuiltInEnclosure } = reservation;

    if (heating) {
      pricePerDay += 50;
    }

    if (outdoor_enclosure && !hasBuiltInEnclosure) {
      pricePerDay += 100;
    }

    if (drugs) {
      pricePerDay += 50;
    }

    // is this really needed??
    if (quarantine) {
      pricePerDay += 500;
    }

    return pricePerDay;
  },

  calculatePrice(reservation) {
    const fullPricePerDay = this.calcFullPricePerDay(reservation);
    return reservation.get('daysLength') * fullPricePerDay;
  },

  calcPriceWithoutVat(price) {
    return Math.round((price / 1.21)).toFixed(2);
  }
});
