import { alias, sort } from '@ember/object/computed';
import Controller from '@ember/controller';
import removeDiacritics from 'calendar/utils/remove-diacritics';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({

  showReservationModal: false,
  showNavigationModal: false,
  selectedReservation: null,
  draggedReservation: null,
  unconfirmedReservationId: null,
  isLoading: false,

  queryParams: ['unconfirmedReservationId'],

  dateHelp: service(),

  month: alias('model.month'),
  year: alias('model.year'),

  // reservations: computed.alias('model.reservations'),
  reservations: computed('model.reservations.[]', function() {
    return this.get('model.reservations').toArray();
  }),

  normalizeString(string = '') {
    return removeDiacritics(string.toLocaleLowerCase());
  },

  filteredReservations: computed('reservations.[]', 'searchValue', function() {
    let { searchValue, reservations } = this.getProperties('searchValue', 'reservations');
    searchValue = this.normalizeString(searchValue);
    if (searchValue && searchValue.length > 0) {
      return reservations.filter((reservation) => {
        const searchAttrs = Object.values(reservation.getProperties('dog_name', 'breed', 'owner')).map((attr) => attr || '');
        return searchAttrs.find((attrValue) => {
          return this.normalizeString(attrValue).includes(searchValue);
        });
      });
    }

    return reservations;
  }),

  reservationsEndingInCurrentMonth: computed('reservations.[]', function() {
    return this.get('reservations').filter((reservation) => {
      return this.get('month') === moment(reservation.get('valid_to')).get('month') + 1;
    });
  }),

  incomeForCurrentMonth: computed('reservationsEndingInCurrentMonth.[]', function() {
    return this.get('reservationsEndingInCurrentMonth').reduce(function(total, res) {
      return total + res.get('totalPrice');
    }, 0);
  }),

  kennels: alias('model.kennels'),
  unconfirmedReservation: alias('model.unconfirmedReservation'),

  // kennelsSorting: ['number'],
  sortedKennels: sort('kennels', function(a, b) {
    if (b.get('number') > a.get('number')) {
      return -1;
    } else {
      return 1;
    }
  }),

  findFreeKennelforNewReservation() {
    let res = this.get('unconfirmedReservation');

    if (res) {
      let kennelType = res.get('kennel_type');
      let potentialKennels = this.get('kennels').filterBy('kennel_type', kennelType);

      let options = [];

      potentialKennels.forEach((kennel) => {
        res.set('idkennel', kennel);
        res.updateOverlappings();
        options.push({
          kennel,
          overlappingDays: res.get('overlappingDays')
        });

      });

      const [bestOption] = options.sortBy('overlappingDays');

      res.set('idkennel', bestOption.kennel);
      res.updateOverlappings();

    }
  },

  nextMonth: computed('month', function() {
    let currentMonth = this.get('month');
    if (currentMonth === 12) {
      return 1;
    }
    return currentMonth + 1;
  }),

  previousMonth: computed('month', function() {
    let currentMonth = Number(this.get('month'));
    if (currentMonth === 1) {
      return 12;
    }
    return currentMonth - 1;
  }),

  nextYear: computed('year', function() {
    return this.get('year') + 1;
  }),

  yearOfNextMonth: computed('month', 'year', function() {
    if (this.get('month') === 12) {
      return this.get('nextYear');
    }
    return this.get('year');
  }),

  yearOfPreviousMonth: computed('month', 'year', function() {
    if (this.get('month') === 1) {
      return this.get('previousYear');
    }
    return this.get('year');
  }),

  previousYear: computed('year', function() {
    return this.get('year') - 1;
  }),

  daysInMonth: computed('month', 'year', function() {
    return this.get('dateHelp').getDaysInMonth(this.get('year'), this.get('month'));
  }),

  daysInTwoMonths: computed('nextMonth', 'yearOfNextMonth', 'daysInMonth', function() {
    let daysInNextMonth = this.get('dateHelp').getDaysInMonth(this.get('nextMonth'), this.get('yearOfNextMonth'));
    return this.get('daysInMonth') + daysInNextMonth;
  }),

  columns: computed('daysInTwoMonths', function() {
    let numberOfDays = this.get('daysInTwoMonths');
    let columns = [];

    for (let i = 1; i <= numberOfDays; i++) {
      columns.push(new Date(this.get('year'), this.get('month') - 1, i));
    }

    return columns;
  }),

  actions: {
    showReservationModal(model) {
      this.set('selectedReservation', model);
      this.set('showReservationModal', true);
    },
    showNavigationModal() {
      this.set('showNavigationModal', true);
    },
    setDraggedReservation(model) {
      this.set('draggedReservation', model);
    },
    closeModal() {
      this.set('showReservationModal', false);
      this.set('showNavigationModal', false);
    },
    saveSelectedReservation() {
      this.get('selectedReservation').save();
    },
    async saveCutReservation(data) {
      let newReservation = this.store.createRecord('reservation', data);
      await newReservation.save();

      this.get('reservations.[]').pushObject(newReservation);
      this.send('saveSelectedReservation');
    }
  }

});
