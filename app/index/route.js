import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import moment from 'moment';

export default Route.extend({

  unconfirmedReservationId: null,

  beforeModel(transition) {
    this.set('unconfirmedReservationId', transition.queryParams.unconfirmedReservationId);
  },

  model(params) {
    let { year, month } = params;
    month = Number(month);

    let unconfirmedReservation = null;
    if (this.get('unconfirmedReservationId')) {
      unconfirmedReservation = this.store.findRecord('reservation', this.get('unconfirmedReservationId'));
    }

    let from = moment([year, month - 1, 1]).format('YYYY-MM-DD');
    let to = moment([year, month - 1, 1]).add(1, 'month').endOf('month').format('YYYY-MM-DD');

    return RSVP.hash({
      kennels: this.modelFor('application'),
      reservations: this.store.query('reservation', { from_date: from, to_date: to }),
      unconfirmedReservation,
      month: Number(params.month),
      year: Number(params.year)
    });
  },

  afterModel() {
    this.controllerFor('index').findFreeKennelforNewReservation();
  },

  actions: {
    didTransition() {
      this.get('controller').send('closeModal');
      this.get('controller').set('isLoading', false);
      this.get('controller').findFreeKennelforNewReservation();
    },

    loading() {
      let controller = this.controllerFor('index');
      controller.set('isLoading', true);
    }
  }

});
