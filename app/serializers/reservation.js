import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalize(modelClass, resourceHash, prop) {
    if (resourceHash.idkennel === 0 || resourceHash.idkennel === null) {
      resourceHash.idkennel = 1;
    }

    return this._super(modelClass, resourceHash, prop);

  }
});
