import { alias } from '@ember/object/computed';
import moment from 'moment';
import { computed } from '@ember/object';
import $ from 'jquery';
import { inject as service } from '@ember/service';
import Controller from '@ember/controller';

export default Controller.extend({
  lang: alias('model.lang'),
  dateHelp: service(),
  sendService: service('send'),
  reject: false,

  init() {
    this._super(...arguments);

    this.set('message', {});
  },

  queryParams: ['reject'],

  currentMonth: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('M');
  }),

  currentYear: computed('model.valid_from', function() {
    return moment(this.get('model.valid_from')).format('YYYY');
  }),

  numberOfDays: computed('model{valid_from,valid_to}', function() {
    return this.get('dateHelp').dateDiff(this.get('model.valid_from'), this.get('model.valid_to'));
  }),

  isResolved: computed('model.status', function() {
    return this.get('model.status') !== 0;
  }),

  actions: {
    async send() {
      const confirm = !this.get('reject');
      let status = +confirm;
      if (status === 0) {
        status = 2;
      }
      const model = this.get('model');

      model.set('status', status);
      await model.save();
      const response = $('.response').val();

      const { message, success } = await this.sendService.makeSendRequest(model.get('id'), response, confirm);
      this.set('message.content', message);
      if (success) {
        this.set('message.class', 'success');
      } else {
        this.set('message.class', 'error');
      }
    }
  }
});
