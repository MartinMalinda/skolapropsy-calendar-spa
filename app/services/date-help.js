import Service from '@ember/service';
import moment from 'moment';

export default Service.extend({
  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  },

  dateDiff(date1, date2, strict) {
    if (date1 === date2 && strict) {
      return 1;
    }

    const mdate1 = moment(date2);
    const mdate2 = moment(date1);

    return mdate1.diff(mdate2, 'days');

  },

  getArrayOfDates(startDate, endDate) {
    const dates = [];
    const start = moment(startDate);
    const end = moment(endDate);

    while (start.unix() <= end.unix()) {
      dates.push(start.format());
      start.add(1, 'day');
    }

    dates.pop();

    return dates;

  },

  overlaps(range1, range2) {
    const overlap = range1.overlaps(range2);
    if (!overlap) {
      return (this.rangesCollideOnFirstDay(range1, range2) || this.rangesCollideOnLastDay(range1, range2));
    } else {
      return overlap;
    }

  },

  rangesCollideOnFirstDay(range1, range2) {
    return (range1.start.isSame(range2.end));
  },

  rangesCollideOnLastDay(range1, range2) {
    return (range1.end.isSame(range2.end));
  },

  intersect(range1, range2) {
    const intersect = range1.intersect(range2);
    if (!intersect) {

      if (this.rangesCollideOnFirstDay) {
        return moment.range(range1.start, range1.start);
      }
      if (this.rangesCollideOnLastDay) {
        return moment.range(range1.end, range1.end);
      }

      return null;

    } else {
      return intersect;
    }
  }
});
