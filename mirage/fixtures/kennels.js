export default [
{
  "id": 1,
  "name": "kotec 1",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 2,
  "name": "kotec 2",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 3,
  "name": "kotec 3",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 4,
  "name": "kotec 4",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 5,
  "name": "kotec 5",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 6,
  "name": "kotec 6",
  "idkennel_type": 5,
  "kennel_type": "Kotec pro malé psy (s výběhem)",
  "color": "#ab3dab",
  "outdoor_enclosure": 1
},
{
  "id": 7,
  "name": "kotec 7",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 8,
  "name": "kotec 8",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 9,
  "name": "kotec 9",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 10,
  "name": "kotec 10",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 11,
  "name": "kotec 11",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 12,
  "name": "kotec 12",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 13,
  "name": "kotec 13",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 14,
  "name": "kotec 14",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 15,
  "name": "kotec 15",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 16,
  "name": "kotec 16",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 17,
  "name": "kotec 17",
  "idkennel_type": 1,
  "kennel_type": "2x2 m",
  "color": "#6ad239",
  "outdoor_enclosure": 0
},
{
  "id": 18,
  "name": "kotec 18",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 19,
  "name": "kotec 19",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 20,
  "name": "kotec 20",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 21,
  "name": "kotec 21",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 22,
  "name": "kotec 22",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 23,
  "name": "kotec 23",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 24,
  "name": "kotec 24",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 25,
  "name": "kotec 25",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 26,
  "name": "kotec 26",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 27,
  "name": "kotec 27",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 28,
  "name": "kotec 28",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 29,
  "name": "kotec 29",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 30,
  "name": "kotec 30",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 31,
  "name": "kotec 31",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 32,
  "name": "kotec 32",
  "idkennel_type": 2,
  "kennel_type": "3x2 m",
  "color": "#74284d",
  "outdoor_enclosure": 0
},
{
  "id": 33,
  "name": "kotec 33",
  "idkennel_type": 3,
  "kennel_type": "4x2 m",
  "color": "#cecebf",
  "outdoor_enclosure": 0
},
{
  "id": 34,
  "name": "kotec 34",
  "idkennel_type": 3,
  "kennel_type": "4x2 m",
  "color": "#cecebf",
  "outdoor_enclosure": 0
},
{
  "id": 35,
  "name": "kotec 35",
  "idkennel_type": 3,
  "kennel_type": "4x2 m",
  "color": "#cecebf",
  "outdoor_enclosure": 0
},
{
  "id": 36,
  "name": "kotec 36",
  "idkennel_type": 4,
  "kennel_type": "Kotec pro 2 psy s výběhem",
  "color": "#4abb08",
  "outdoor_enclosure": 1
},
{
  "id": 37,
  "name": "kotec 37",
  "idkennel_type": 4,
  "kennel_type": "Kotec pro 2 psy s výběhem",
  "color": "#4abb08",
  "outdoor_enclosure": 1
},
{
  "id": 38,
  "name": "kotec 38",
  "idkennel_type": 4,
  "kennel_type": "Kotec pro 2 psy s výběhem",
  "color": "#4abb08",
  "outdoor_enclosure": 1
},
{
  "id": 39,
  "name": "kotec 39",
  "idkennel_type": 6,
  "kennel_type": "Karanténa",
  "color": "#db0027",
  "outdoor_enclosure": 0
}
]
