export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */
  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Route shorthand cheatsheet
  */
  
    // GET shorthands

    // Collections
    this.get('/api/reservations', 'reservations');
    this.get('/api/kennels', 'kennels');
    // this.get('/contacts', 'users');
    // this.get('/contacts', ['contacts', 'addresses']);

    // Single objects
    // this.get('/contacts/:id');
    // this.get('/contacts/:id', 'user');
    // this.get('/contacts/:id', ['contact', 'addresses']);
    this.get('/api/reservation/:id',  function(db, request){
      return { 
        'reservation': db.reservations.find(request.params.id)
      };
    }); // specify the type of resource to be updated
  

  
    // POST shorthands

    this.post('/api/reservations', 'reservation');
    // this.post('/contacts', 'user'); // specify the type of resource to be created
  
  this.post('/admin/calendar/api/send', function(){
    return { success: true };
  });

  this.post('/admin/invoice/:id', function(db, request) {
    const res = db.reservations.find(request.params.id);
    res.update({ issued_invocie: true });
    res.save();
    return {};
  });

  
    // PUT shorthands

    this.put('/api/reservations/:id');
  

  /*
    DELETE shorthands

    this.del('/contacts/:id');
    this.del('/contacts/:id', 'user'); // specify the type of resource to be deleted

    // Single object + related resources. Make sure parent resource is first.
    this.del('/contacts/:id', ['contact', 'addresses']);
  */

  /*
    Function fallback. Manipulate data in the db via

      - db.{collection}
      - db.{collection}.find(id)
      - db.{collection}.where(query)
      - db.{collection}.update(target, attrs)
      - db.{collection}.remove(target)

    // Example: return a single object with related models
    this.get('/contacts/:id', function(db, request) {
      var contactId = +request.params.id;

      return {
        contact: db.contacts.find(contactId),
        addresses: db.addresses.where({contact_id: contactId})
      };
    });

  */
}

/*
You can optionally export a config that is only loaded during tests
export function testConfig() {

}
*/
