import removeDiacritics from 'calendar/utils/remove-diacritics';
import { module, test } from 'qunit';

module('Unit | Utility | remove diacritics');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = removeDiacritics();
  assert.ok(result);
});
