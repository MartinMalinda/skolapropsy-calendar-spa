import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('overlap-highlighter', 'Integration | Component | overlap highlighter', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{overlap-highlighter}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#overlap-highlighter}}
      template block text
    {{/overlap-highlighter}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
