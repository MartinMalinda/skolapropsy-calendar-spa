/* eslint-env node */
'use strict';

module.exports = function(environment) {
  var ENV = {

    environment,

    rowHeight: 24,
    colWidth: 40,

    modulePrefix: 'calendar',
    rootElement: '#calendar',
    rootURL: '/admin/calendar/',
    locationType: 'auto',
    // endpoint: 'http://test.skolapropsy.cz/admin/calendar',
    endpoint: 'http://localhost:4200',
    domInit: false,
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    contentSecurityPolicy: {
      'font-src': "'self' fonts.gstatic.com http://skolapropsy.cz data:",
      'style-src': "'self' 'unsafe-inline' fonts.googleapis.com http://skolapropsy.cz",
      'img-src': "'self' www.gravatar.com placeholdit.imgix.net *.placehold.it www.filepicker.io *.amazonaws.com",
      'default-src': 'none',
      'frame-src': '*',
      'script-src':"'self' http://skolapropsy.cz"
    },

    moment: {
      outputFormat: 'YYYY-MM-DD'
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.rootElement = '#calendar';
    ENV.rootURL = '/admin/calendar/';
    ENV.endpoint = '/admin/calendar';
    ENV.domInit = true;
    // ENV.baseUrl = '/a';
  }

  return ENV;
};
